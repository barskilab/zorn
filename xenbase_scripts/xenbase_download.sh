#!/bin/bash
if [ $# -eq 0 ]
then
    echo "Usage:"
    echo "./download.sh SraRunTable.txt output_folder"
    exit 0
fi
cat $1 | awk -F $'\t' 'FNR == 1 {next} {print "ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByStudy/sra/SRP/"substr($25,1,6)"/"$25"/"$6"/"$6".sra"}' | xargs wget -N -P $2
echo "Done"
