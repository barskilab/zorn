#!/usr/bin/python

import sys
import re
import os
import shutil
import argparse
from custom_functions import (normalize_args,
                              open_file,
                              parse_sra_file,
                              get_files_from_folder)


FOLDERS = {
    "RSEM":   {"folder_name": 'rsem_results', "pattern": ".*isoforms.results.*|.*genes.results.*"},
    "BIGWIG": {"folder_name": 'bigwig',       "pattern": ".*bigwig.*"},
    "BAM":    {"folder_name": 'bambai',       "pattern": ".*bam.*"}
}


def get_arg_parser():
    """Returns argument parser"""
    general_parser = argparse.ArgumentParser(description='rename', add_help=True)
    general_parser.add_argument("-s", "--sratable",  help="Path to SraRunTable description file", required=True)
    general_parser.add_argument("-i", "--input",     help="Path to the input directory",          required=True)
    general_parser.add_argument("-o", "--output",    help="Path to the output directory",         required=True)
    return general_parser


def create_folders (sra_run_table_dict, outdir):
    """Creates the outputs folders structure based on FOLDERS"""
    for value in [os.path.join(outdir, details["folder_name"]) for details in FOLDERS.values()]:
        print "Created output folder", value
        os.makedirs(value)


def save_to_folders(sra_run_table_dict, input_dir, output_dir):
    """Copies input files to the folders on the base on FOLDERS values"""
    files_dict = get_files_from_folder(input_dir)
    total = len(files_dict)
    counter = 0.0
    for short_filename, long_filename in files_dict.iteritems():
        counter += 1
        print "Progress:", counter/total*100
        try:
            sra_id = re.findall("^SRR[0-9]{1,10}", short_filename)[0]
            for key,value in FOLDERS.iteritems():
                if re.match(value["pattern"], short_filename, re.IGNORECASE):
                    detailed_name = re.sub('^SRR[0-9]{1,10}', sra_run_table_dict[sra_id]['Sample_Name'], short_filename)
                    shutil.copyfile(long_filename, os.path.join(output_dir, value["folder_name"], detailed_name))
                    print "Copy", long_filename, "to", os.path.join(output_dir, value["folder_name"], detailed_name)
        except KeyError as ex:
            print "Skip: ", short_filename, "is not present in SraRunTable", ex
        except IndexError as ex:
            print "Skip: ", short_filename, "couldn't get SRR id from the file name", ex
        except IOError as ex:
            print "Skip: ", short_filename, "check permissions", ex


def main(argsl=None):
    if argsl is None:
        argsl = sys.argv[1:]
    args,_ = get_arg_parser().parse_known_args(argsl)

    try:
        args = normalize_args(args)
        sra_run_table_dict = parse_sra_file(file_content_list=open_file(args.sratable),
                                            key_field='Run')
        create_folders (sra_run_table_dict, args.output)
        save_to_folders (sra_run_table_dict, args.input, args.output)
    except Exception as ex:
        print ex
        sys.exit(1)


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
