#!/usr/bin/python
import argparse
import os
from custom_exceptions import BadFormatException


def normalize_args(args, skip_list=[]):
    """Converts all relative path arguments to absolute ones relatively to the current working directory"""
    normalized_args = {}
    for key,value in args.__dict__.iteritems():
        if key not in skip_list:
            normalized_args[key] = value if not value or os.path.isabs(value) else os.path.normpath(os.path.join(os.getcwd(), value))
        else:
            normalized_args[key]=value
    return argparse.Namespace (**normalized_args)


def open_file(filename):
    """Returns list of lines from the text file. \n at the end of the lines are trimmed"""
    lines = []
    with open(filename, 'r') as infile:
        for line in infile:
            lines.append(line.strip())
    return lines


def skip_line (line, header, key_field):
    """
    Checks if line should be skipped, when it's empty or header is found.
    Raises BadFormatException if header is absent in a line when the function is called for the first time or
    when duplicate header is found. Header returned from the function should be set as an input parameter when
    this function is called in a loop.
    :param line: string from the file
    :param header: header line of the current file. Should be None when function is called for the first time
    :param key_field: string that should be present only in the header line 
    :return: Boolean to define if line should be skipped or not, header line
    """
    if line == '':
        print "   skip empty line"
        return True, header
    if key_field in line.split('\t'):
        if header is None:
            header = line.split('\t')
            return True, header
        else:
            raise BadFormatException(message="Bad format: duplicate header line")
    if header is None:
        raise BadFormatException(message="Bad format: Couldn't find header in the first not empty line")
    return False, header


def parse_sra_file (file_content_list, key_field):
    """
    Parses content of SraRunTable file into {key_field: {columnName: fieldValue}} dictionary.
    If header is not found, duplicate header (raise from skip_line_res), duplicate key_field or duplicate column name
    raise BadFormatException.
    :param file_content_list: list of the lines from SraRunTable
    :param key_field: column name from SraRunTable file to be used as a key in a resulting dictionary
    :return: {key_field: {columnName: fieldValue}} dictionary for parsed SraRunTable file
    """
    file_content_dict = {}
    header = None
    for line in file_content_list:
        skip_line_res = skip_line(line, header, key_field)
        if skip_line_res[0]:          # means we need to skip this line
            header = skip_line_res[1] # header could be still None or already changed into real header from file
            continue
        line_splitted = line.split('\t') # should be \t, because some of the values include spaces
        key_index = header.index(key_field)
        if line_splitted[key_index] in file_content_dict.keys():
            # Duplicate key_field, shouldn't be like this
            raise BadFormatException (message="Bad format: dupclicate {}".format(line_splitted[key_index]))
        value_dict = {}
        for idx,value in enumerate(line_splitted):
            if idx == key_index:
                continue  # skip the key_index
            if header[idx] in value_dict.keys():
                raise BadFormatException(message="Bad format: duplicate column name {}".format(header[idx]))
            value_dict[header[idx]] = value
        file_content_dict[line_splitted[key_index]] = value_dict
    return file_content_dict


def get_files_from_folder(input_dir):
    """
    Recursibvly returns dictionary of filenames from a folder in a form of {shortName: fullname}.
    If it's a single file, not a folder, return dictionary with only one item
    :param input_dir: folder to search for files
    :return: {shortName: fullname} dictionary for all find found in input_dir and its subdirectories
    """
    files_dict = {}
    if os.path.isdir(input_dir):
        for root, dirs, files in os.walk(input_dir):
            files_dict.update({filename: os.path.join(root, filename) for filename in files})
    else:
        files_dict.update({os.path.basename(input_dir): input_dir})
    return files_dict


def flatten(input_list):
    result = []
    for i in input_list:
        if isinstance(i,list) or isinstance(i,tuple): result.extend(flatten(i))
        else: result.append(i)
    return result


def export_to_file (output_filename, data):
    with open(output_filename, 'w') as output_file:
        output_file.write(data)


def print_sra_run_table (sra_run_table_dict):
    for key, value in sra_run_table_dict.iteritems():
        print key, "\n"
        for inner_key, inner_value in value.iteritems():
            print '   ',inner_key+": ","["+inner_value+"]"
        print '\n'
