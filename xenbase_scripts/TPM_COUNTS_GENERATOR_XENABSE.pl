##### Copyright Praneet Chaturvedi [Senior Analyst Bioinformatics, CCHMC] #######
print "TPM\_COUNTS\_GENERATOR\_XENABSE\.pl is developed and maintained by Praneet Chaturvedi\n";
print "\nVersion \-\- 1.01\n";
print "\nVersion updates from previous version 1.0 are : removing auto filter and producing geneid instead of gene names\n";
sleep(2);
use List::Util qw(sum);
my $dir = $ARGV[0]; chomp $dir; ######################## Directory of Isoforms results files.
my @samples;my %tpm_hash;my %counts_hash;my $header;my %res_tpm; my %res_counts;
opendir(F,"$dir");
foreach my $f(readdir(F))
{
 if($f =~ /.isoforms.results/)
 {
  my @arrf=split(/\.isoforms.results/,$f);
  push(@samples,$arrf[0]);
  open(R,"$dir/$f");
  while(my $data = <R>)
  {
   chomp $data;
   my @arr=split(/\t/,$data);
   my $var=$arr[1]."\<".$arr[0]."\>";
   my $value1=$arrf[0]."\t".$arr[5]; ############## Storing TPM VALUES
   $tpm_hash{$var}{$value1}=0;
   my $value2=$arrf[0]."\t".int($arr[4]); ############## Storing COUNTS VALUES
   $counts_hash{$var}{$value2}=0;
  }
  close R;
 }
}
closedir F;
foreach my $m(@samples)
{
 if($m eq "")
 {}
 else
 {
  $header = $header."\t".$m;
 }
}
# print "\nProcessed header \- $header\n";
my $mainheader = "Gene"."\<"."Transcript"."\>".$header;
foreach my $m(keys %tpm_hash)
{
 my $finalval = "";
 foreach my $a(@samples)
 {
  my $val = "";
  foreach my $n(keys %{$tpm_hash{$m}})
  {
   my @arr=split(/\t/,$n);
   if($a eq $arr[0])
   {
    $val = $arr[1];
   }
  }
  if($val eq "")
  {
   $finalval=$finalval."\t"."NA";
  }
  else
  {
   $finalval=$finalval."\t".$val;
  }
 }
 $res_tpm{$m}{$finalval}=0;
}
foreach my $m(keys %counts_hash)
{
 my $finalval = "";
 foreach my $a(@samples)
 {
  my $val = "";
  foreach my $n(keys %{$counts_hash{$m}})
  {
   my @arr=split(/\t/,$n);
   if($a eq $arr[0])
   {
    $val = $arr[1];
   }
  }
  if($val eq "")
  {
   $finalval=$finalval."\t"."NA";
  }
  else
  {
   $finalval=$finalval."\t".$val;
  }
 }
 $res_counts{$m}{$finalval}=0;
}
open(OUT,">Isoforms_TPM_matrix.txt");
print OUT "$mainheader\n";
foreach my $m(keys %res_tpm)
{
 foreach my $n(keys %{$res_tpm{$m}})
 {
  # print "$m$n\n";
  print OUT "$m$n\n";
 }
}
close OUT;
open(OUT,">Isoforms_Counts_matrix.txt");
print OUT "$mainheader\n";
foreach my $m(keys %res_counts)
{
 foreach my $n(keys %{$res_counts{$m}})
 {
  # print "$m$n\n";
  print OUT "$m$n\n";
 }
}
close OUT;
my $header_new;my %hash_new;my $length_new;
open(F,"Isoforms_TPM_matrix.txt");
while(my $data =<F>)
{
 $data =~ s/^\s+|\s+$//g;
 chomp $data;
 if($data =~/Gene<Transcript>/)
 {
  my @arr=split(/\t/,$data,2);
  $header_new = $arr[1];
 }
 else
 {
  my @arr=split(/\t/,$data,2);
  my @arr1=split(/\</,$arr[0]);
  my $var=$arr1[1]."\t".$arr[1];
  $hash_new{$arr1[0]}{$var}=0;
  my @len=split(/\t/,$arr[1]);
  $length_new=scalar(@len);
 }
}
close F;
open(OUT,">Genes_TPM_Matrix.txt");
print OUT "Gene\t$header_new\n";
foreach my $m(keys %hash_new)
{
 if($m eq "na")
 {}
 else
 {
  my $expression="";
  for(my $i=0;$i<=($length_new-1);$i++)
  {
   my $value=0;
   foreach my $n(keys %{$hash_new{$m}})
   {
    my @arr=split(/\t/,$n);
    shift(@arr);
    $value=$value+$arr[$i];
   }
   my $newval=$value;
   $expression=$expression."\t".$newval;
  }
  my @arrexp=split(/\t/,$expression);
  shift(@arrexp);
  my $sumofexp=sum(@arrexp);
  # print "$m$expression\n";
  print OUT "$m$expression\n";
 }
}
close OUT;
my $header_new1;my %hash_new1;my $length_new1;
open(F,"Isoforms_Counts_matrix.txt");
while(my $data =<F>)
{
 $data =~ s/^\s+|\s+$//g;
 chomp $data;
 if($data =~/Gene<Transcript>/)
 {
  my @arr=split(/\t/,$data,2);
  $header_new1 = $arr[1];
 }
 else
 {
  my @arr=split(/\t/,$data,2);
  my @arr1=split(/\</,$arr[0]);
  my $var=$arr1[1]."\t".$arr[1];
  $hash_new1{$arr1[0]}{$var}=0;
  my @len=split(/\t/,$arr[1]);
  $length_new1=scalar(@len);
 }
}
close F;
open(OUT,">Genes_Counts_Matrix.txt");
print OUT "Gene\t$header_new1\n";
foreach my $m(keys %hash_new1)
{
 if($m eq "na")
 {}
 else
 {
  my $expression="";
  for(my $i=0;$i<=($length_new1-1);$i++)
  {
   my $value=0;
   foreach my $n(keys %{$hash_new1{$m}})
   {
    my @arr=split(/\t/,$n);
    shift(@arr);
    $value=$value+$arr[$i];
   }
   my $newval=$value;
   $expression=$expression."\t".$newval;
  }
  my @arrexp=split(/\t/,$expression);
  shift(@arrexp);
  my $sumofexp=sum(@arrexp);
  # print "$m$expression\n";
  print OUT "$m$expression\n";
 }
}
close OUT;
print "\nAll done and dusted\.\.\. Please contact Praneet Chaturvedi for issues and bugs\n";
