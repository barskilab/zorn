#!/usr/bin/python
import sys
import os
import argparse
import subprocess
from custom_exceptions import (CustomException,
                               BadFormatException)
from custom_functions import (normalize_args,
                              open_file,
                              parse_sra_file,
                              get_files_from_folder,
                              skip_line,
                              flatten,
                              export_to_file)


def get_arg_parser():
    """Returns argument parser"""
    general_parser = argparse.ArgumentParser(description='rnaseq_combine', add_help=True)
    general_parser.add_argument("-g", "--gsm",    help="Path to the file with GSM control & treatment relation table", required=True)
    general_parser.add_argument("-s", "--sra",    help="Path to the folder with SraRunTable description files or to the single SraRunTable description file", required=True)
    general_parser.add_argument("-i", "--input",  help="Path to the folder with gene count files", required=True)
    general_parser.add_argument("-o", "--output", help="Path to the output folder", required=True)
    general_parser.add_argument("-d", "--deseq",  help="Run deseq2.R script with exported results", action="store_true")
    return general_parser


def parse_sra_folder(sra_folder_location, key_field):
    """
    Loads all SraRunTable files from sra_folder_location into gsm_dict dictionary.
    If failed to load some SraRunTable file, skip it
    :param sra_folder_location: path the folder where all SraRunTable files are stored
    :param key_field: column name from SraRunTable file to be used as a key in a resulting dictionary
    :return: {GSM_ID: {column_name: field_value}} combined dictionary for all loaded SraRunTable files
    """
    sra_files_dict = get_files_from_folder (sra_folder_location)
    gsm_dict = {}  # {GSM_ID: {column_name: field_value}}
    for filename, location in sra_files_dict.iteritems():
        print "Loading ", filename
        try:
            sra_run_table_dict = parse_sra_file(file_content_list=open_file(location), key_field=key_field)
            gsm_dict.update(sra_run_table_dict)
        except BadFormatException as ex:
            print "Failed to load", filename, "skipping"
    return gsm_dict


def parse_treatment_vs_control_file (treatment_vs_control_file_content_list, key_field):
    """
    Parses treatment_vs_control file into dictionary to include array of tuples with
    treatment GSM_ID's vs control GSM_ID's for each GSE_ID.
    Skip all the lines which doesn't have either treatment or control column.
    Raise BadFormatException from skip_line function if header is not found or duplicate header line.
    :param treatment_vs_control_file_content_list: list of strings from treatment_vs_control file
    :param key_field: column name from treatment_vs_control file to be used as a key in a resulting dictionary
    :return: { GSE_ID: [([GSM_ID_treatment], [GSM_ID_controls])]} dictionary for parsed file content
    """
    treatment_vs_control_list = {}  # { GSE_ID: [([GSM_ID_treatment], [GSM_ID_controls])]}
    header = None
    for line in treatment_vs_control_file_content_list:
        skip_line_res = skip_line(line, header, key_field)
        if skip_line_res[0]:           # means we need to skip this line
            header = skip_line_res[1]  # Header could be still None or already changed into real header from file
            continue
        line_splitted = line.split('\t')  # should be \t, because some of the values include spaces
        try:
            treatment = [item.strip() for item in line_splitted[2].split(',')]
            controls = [item.strip() for item in line_splitted[3].split(',')]
        except IndexError as ex:
            continue
        key_index = header.index(key_field)
        if line_splitted[key_index] in treatment_vs_control_list.keys():
            #  Duplicate key_field, should update dict
            treatment_vs_control_list[line_splitted[key_index]].append((treatment, controls))
        else:
            treatment_vs_control_list[line_splitted[key_index]] = [(treatment, controls)]
    return treatment_vs_control_list


def parse_gene_counts_file(gene_counts_file_content, key_field, srr_gsm_filtered_dict):
    """
    Parses Genes_Count_Matrix file into { GSM_ID: [gene_count_data] } dictionary, skips all the columns with the
    name that doesn't include any SRR_ID from srr_gsm_filtered_dict.keys() and any GSM_ID from srr_gsm_filtered_dict.values().
    Raise BadFormatException from skip_line function if header is not found or duplicate header line.
    Raise BadFormatException when duplicate key_field is found - duplicate gene_id.
    :param gene_counts_file_content: list of strings from Genes_Count_Matrix file
    :param key_field: column name from Genes_Count_Matrix file to be used as a key in a resulting dictionary
    :param srr_gsm_filtered_dict: { SRR_ID: GSM_ID } dictionary to include SRR_ID which should be loaded from Genes_Count_Matrix file
    :return: ([], { GSM_ID: [gene_count_data] }) gene_list with all of the gene ids and dictionary with
             gene_count_data list sorted in the same order as gene_list for each experiment present
             in srr_gsm_filtered_dict
    """
    gene_counts_dict = {}  # { GSM_ID: [gene_count_data] }
    gene_list = []
    header = None
    for line in gene_counts_file_content:
        skip_line_res = skip_line(line, header, key_field)
        if skip_line_res[0]:           # means we need to skip this line
            header = skip_line_res[1]  # Header could be still None or already changed into real header from file
            key_index = header.index(key_field)
            continue
        line_splitted = line.split('\t')  # should be \t, because some of the values include spaces
        if line_splitted[key_index] in gene_list:
            # Duplicate key_field, shouldn't be like this. In this case it means duplicate gene_id
            raise BadFormatException(message="Bad format: dupclicate {}".format(line_splitted[key_index]))
        gene_list.append(line_splitted[key_index])  # add gene name
        for idx, value in enumerate(line_splitted):
            srr_id, gsm_id = next(
                (srr_gsm_item for srr_gsm_item in srr_gsm_filtered_dict.items() if srr_gsm_item[0] in header[idx] or srr_gsm_item[1] in header[idx]),
                (None, None))
            if idx == key_index or not srr_id or not gsm_id:
                continue  # skip the key_index or when SRR or GSM id is not present in srr_gsm_filtered_dict
            if gsm_id in gene_counts_dict.keys():
                gene_counts_dict[gsm_id].append(value)
            else:
                gene_counts_dict[gsm_id]=[value]
    return gene_list, gene_counts_dict


def make_counts_data(treatment_vs_control_tuple, gene_list, gene_counts_dict):
    """
    Generates data to export. Selects data from gene_counts_dict by GSM_ID according to treatment_vs_control_tuple.
    Exports Control vs Treatment columns
    :param treatment_vs_control_tuple: ( [GSM_treatment], [GSM_controls] ) GSM_ID's correspondence for current data export
    :param gene_list: [] gene list which defined the order of gene expression data in gene_count_data in gene_counts_dict
    :param gene_counts_dict: { GSM_ID: [gene_count_data] } - dictionary for all GSM_ID loaded from Genes_Count_Matrix file
                             for current GSE_ID
    :return: tab-delimited data 
    """
    treatments = treatment_vs_control_tuple[0]
    controls = treatment_vs_control_tuple[1]
    header = '\t'.join(flatten([
                                    'Gene',
                                    [ item for item in controls ],
                                    [ item for item in treatments ]
                               ]))
    data = header + '\n'
    for idx, gene_id in enumerate(gene_list):
        line = gene_id + '\t'
        for gsm_control_id in controls:
            line += gene_counts_dict[gsm_control_id][idx] + '\t'
        for gsm_treatment_id in treatments:
            line += gene_counts_dict[gsm_treatment_id][idx] + '\t'
        data += line.strip() + '\n'  # need to strip() line to delete final \t
    return data


def make_group_data(treatment_vs_control_tuple):
    data = '\t'.join(('experiment', 'group')) + '\n'
    for control in treatment_vs_control_tuple[1]:       # controls
        data += '\t'.join((control, 'control')) + '\n'
    for treatment in treatment_vs_control_tuple[0]:     # treatments
        data += '\t'.join((treatment, 'treatment')) + '\n'
    return data


def run_deseq(counts_data_file, group_data_file):
    deseq_results_file = os.path.splitext(counts_data_file)[0] + '.deseq'
    deseq_exec = os.path.join(os.path.dirname(__file__), 'deseq2.R')
    subprocess.check_call([deseq_exec, counts_data_file, group_data_file, deseq_results_file])


def export_results(gse_id, treatment_vs_control_list, gene_list, gene_counts_dict, output_folder, args):
    """
    Generates and exports results for each of the treatment vs control correspondence from treatment_vs_control_list.
    For each GSE_ID new subfolder in output_folder is created.
    Filename and output data has different order: control vs treatment.
    If failed to export data, skip file
    :param gse_id: 
    :param treatment_vs_control_list: [ ([GSM_treatment], [GSM_controls]) ] list of correspondences between treatment and
                                      control GSM_ID's
    :param gene_list: [] list of genes obtainde from Genes_Count_Matrix file for current GSE_ID 
    :param gene_counts_dict: { GSM_ID: [gene_count_data] } - dictionary for all GSM_ID loaded from Genes_Count_Matrix file
                             for current GSE_ID
    :param output_folder: path to export results
    :return: None
    """
    print "Exporting results for", gse_id
    for treatment_vs_control_tuple in treatment_vs_control_list:
        subfolder_name = os.path.join(output_folder, gse_id)
        if not os.path.exists(subfolder_name):
            os.makedirs(subfolder_name)
        basename_root = "-".join(treatment_vs_control_tuple[1]) + "-vs-" + "-".join(treatment_vs_control_tuple[0])
        counts_data = make_counts_data(treatment_vs_control_tuple=treatment_vs_control_tuple,
                         gene_list=gene_list,
                         gene_counts_dict=gene_counts_dict)
        group_data = make_group_data(treatment_vs_control_tuple)
        try:
            counts_data_file = os.path.join(subfolder_name, basename_root+'.txt')
            group_data_file = os.path.join(subfolder_name, basename_root+'.group')
            export_to_file (counts_data_file, counts_data)
            export_to_file(group_data_file, group_data)
            if args.deseq:
                run_deseq(counts_data_file, group_data_file)
        except IOError as ex:
            print "Failed to export data", ex
        except subprocess.CalledProcessError as ex:
            print "Failed to run deseq for", counts_data_file, group_data_file, ex


def main(argsl=None):
    if argsl is None:
        argsl = sys.argv[1:]
    args,_ = get_arg_parser().parse_known_args(argsl)

    try:
        args = normalize_args(args, ['deseq'])

        # { GSM_ID: {column_name: field_value} }
        # dictionary for all of the SraRunTable's in a folder set as --sra
        gsm_dict = parse_sra_folder(sra_folder_location=args.sra,
                                    key_field='Sample_Name')

        # { GSE_ID: [([GSM_ID_treatment], [GSM_ID_controls])]}
        # dictionary to include array of tuples with treatment GSM_ID's vs control GSM_ID's for each GSE_ID
        treatment_vs_control_dict = parse_treatment_vs_control_file (treatment_vs_control_file_content_list=open_file(args.gsm),
                                                                     key_field='GSE')

        # print "treatment_vs_control_dict"
        # for gse_id, treatment_vs_control_list in treatment_vs_control_dict.iteritems():
        #     print gse_id
        #     for treatment_vs_control_tuple in treatment_vs_control_list:
        #         print "  ", treatment_vs_control_tuple[0], "-", treatment_vs_control_tuple[1]

        # {GSE: filename}
        # Dictionary to include full filenames for all of files present in --input directory which has the name that
        # includes GSE_ID's from treatment_vs_control_dict.
        # If args.input is a single file, it's not required for its name to include GSE_ID
        gene_counts_file_dict_filtered = {gse_id: full_name
                                            for gse_id in treatment_vs_control_dict.keys()
                                                for short_name,full_name in get_files_from_folder(args.input).iteritems()
                                                    if gse_id in short_name
                                         }

        # Iterate over the files in gene_counts_file_dict_filtered. Load, process and export resutls
        for gse_id, full_name in gene_counts_file_dict_filtered.iteritems():
            print "Processing genes count data from ", gse_id, ", ", full_name

            # { SRR_ID: GSM_ID } - select only those pairs of SRR_ID: GSM_ID which are present in
            # treatment_vs_control_dict for current GSE_ID
            srr_gsm_filtered_dict = {gsm_dict[gsm_id]['Run']: gsm_id
                                    for gsm_id in list(set(flatten(treatment_vs_control_dict[gse_id]))) }

            # within one Genes_Count_Matrix file which corresponds to one GSE_ID the genes order is constant,
            # so we can save it separately in gene_list
            # { GSM_ID: [gene_count_data] } - dictionary for all GSM_ID loaded from Genes_Count_Matrix file with the
            # list of correspondent gene_count_data
            gene_list, gene_counts_dict = parse_gene_counts_file(gene_counts_file_content=open_file (full_name),
                                                                 key_field="Gene",
                                                                 srr_gsm_filtered_dict=srr_gsm_filtered_dict)
            # export results to files for current GSE_ID
            export_results(gse_id=gse_id,
                           treatment_vs_control_list=treatment_vs_control_dict[gse_id],
                           gene_list=gene_list,
                           gene_counts_dict=gene_counts_dict,
                           output_folder=args.output,
                           args=args)

    except CustomException as ex:
        print ex
    except Exception as ex:
        print ex
        raise


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
