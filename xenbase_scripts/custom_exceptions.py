#!/usr/bin/python


class CustomException(Exception):
    """Custom exception class to identify predictible exceptions from critical ones"""
    def __init__(self, code, message):
        self.code = code
        self.message = message
        super(CustomException, self).__init__(self.message)


class NotSupportedException(CustomException):
    """Feature is not supported"""
    def __init__(self, code=None, message=None):
        code = code if code else 404
        message = message if message else "Not supported feature"
        super(NotSupportedException, self).__init__(code, message)


class BadFormatException(CustomException):
    """Feature is not supported"""
    def __init__(self, code=None, message=None):
        code = code if code else 406
        message = message if message else "Bad formatted file"
        super(BadFormatException, self).__init__(code, message)